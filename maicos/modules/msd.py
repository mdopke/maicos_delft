#!/usr/bin/env python
# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding:utf-8 -*-
#
# Copyright (c) 2020 Authors and contributors
# (see the file AUTHORS for the full list of names)
#
# Released under the GNU Public Licence, v2 or any higher version
# SPDX-License-Identifier: GPL-2.0-or-later

import numpy as np
import scipy.constants
import MDAnalysis as mda

from .base import SingleGroupAnalysisBase, MultiGroupAnalysisBase
from ..utils import check_compound, FT, iFT, savetxt
from ..decorators import charge_neutral

def Bin(a, bins):
    """Averages array values in bins for easier plotting.
    Note: "bins" array should contain the INDEX (integer) where that bin begins"""

    if np.iscomplex(a).any():
        avg = np.zeros(len(bins), dtype=complex)  # average of data
    else:
        avg = np.zeros(len(bins))

    count = np.zeros(len(bins), dtype=int)
    ic = -1

    for i in range(0, len(a)):
        if i in bins:
            ic += 1  # index for new average
        avg[ic] += a[i]
        count[ic] += 1

    return avg / count


# @charge_neutral(filter="default")
class msd_bulk(SingleGroupAnalysisBase):
    """
    """

    def __init__(self,
                 atomgroup,
                 outfreq=10000,
                 output="msd.dat",
                 **kwargs):
        super().__init__(atomgroup, **kwargs)
        self.outfreq = outfreq
        self.output = output

    def _configure_parser(self, parser):
        parser.add_argument('-o', dest='output')

    def _prepare(self):

        self.n_frames = (self.stopframe - self.startframe) // self.step
        self.N = len(self.atomgroup)

        self.msd = np.zeros((self.n_frames, 3))
        self.msd2 = np.zeros((self.n_frames, 3))

    def _single_frame(self):
        if self._frame_index == 0:
            self.positions0 = self.atomgroup.atoms.positions

        this_msd = ((self.atomgroup.atoms.positions - self.positions0)** 2).sum(axis=0) / self.N

        self.msd[self._frame_index, :] += this_msd
        self.msd2[self._frame_index, :] += this_msd * this_msd

        if self._save and self._frame_index % self.outfreq == 0 and self._frame_index > 0:
            self._calculate_results()
            self._save_results()

    def _calculate_results(self):
        self.results["time"] = np.arange(self.n_frames)

        self.results["msdx"] = self.msd[:, 0]
        self.results["msdy"] = self.msd[:, 1]
        self.results["msdz"] = self.msd[:, 2]

        self.results["dmsdx"] = self.msd2[:, 0]
        self.results["dmsdy"] = self.msd2[:, 1]
        self.results["dmsdz"] = self.msd2[:, 2]


    def _conclude(self):
        if self._verbose:
            print("Nothing to be done")

    def _save_results(self):
        outdata = np.hstack([
            self.results["time"][:, np.newaxis],
            self.results["msdx"][:, np.newaxis],
            self.results["dmsdx"][:, np.newaxis],
            self.results["msdy"][:, np.newaxis],
            self.results["dmsdz"][:, np.newaxis],
            self.results["msdz"][:, np.newaxis],
            self.results["dmsdz"][:, np.newaxis]
        ])

        savetxt(self.output,
                outdata,
                header='time,msdx,dmsdx,msdy,dmsdy,msdz,dmsdz')


# @charge_neutral(filter="default")
class msd_planar(SingleGroupAnalysisBase):
    """
    """

    def __init__(self,
                 atomgroups,
                 output_prefix="msd",
                 binwidth=0.05,
                 nbins=-1,
                 dim=2,
                 zmin=0,
                 zmax=-1,
                 zpos=[],
                 outfreq=10000,
                 b2d=False,
                 bsym=False,
                 vac=False,
                 membrane_shift=False,
                 com=False,
                 bpbc=True,
                 nframes=-1,
                 **kwself):
        super().__init__(atomgroups, **kwself)
        self.output_prefix = output_prefix
        self.binwidth = binwidth
        self.dim = dim
        self.zmin = zmin
        self.zmax = zmax
        self.zpos = zpos
        self.outfreq = outfreq
        self.b2d = b2d
        self.bsym = bsym
        self.vac = vac
        self.membrane_shift = membrane_shift
        self.nbins = nbins
        self.nframes = nframes
        # print('In development. EXITING')
        # exit()


    def _configure_parser(self, parser):
        parser.add_argument('-o', dest='output_prefix')
        parser.add_argument('-dz', dest='binwidth')
        parser.add_argument('-nbins', dest='nbins')
        parser.add_argument('-nframes', dest='nframes')
        parser.add_argument('-d', dest='dim')
        parser.add_argument('-zmin', dest='zmin')
        parser.add_argument('-zmax', dest='zmax')
        parser.add_argument('-zpos', dest='zpos')
        parser.add_argument('-temp', dest='temperature')
        parser.add_argument('-dout', dest='outfreq')
        parser.add_argument('-2d', dest='b2d')
        parser.add_argument('-vac', dest='vac')
        parser.add_argument('-sym', dest='bsym')
        parser.add_argument('-shift', dest='membrane_shift')

    def _prepare(self):
        if self._verbose:
            print("\nCalcualate profile for the following group(s):")

        self.zmax = float(self.zmax)
        self.zmin = float(self.zmin)
        self.binwidth = float(self.binwidth)
        self.nbins = int(self.nbins)
        self.dim = int(self.dim)
        self.outfreq = 10000




        self.nframes = int(self.nframes)
        if self.nframes == -1:
            self.nframes = self.n_frames

        # Assume a threedimensional universe...
        self.xydims = np.roll(np.arange(3), -self.dim)[1:]
        dz = self.binwidth

        if self.zmax == -1:
            self.zmax = self._universe.dimensions[self.dim]

        # CAVE: binwidth varies in NPT !
        # Use fix bin width if no nbins are specified
        self.varbins = False
        if self.zpos:
            # only inner bins are defined,
            # final borders are zmin and zmax
            self.zpos = np.asarray(self.zpos.split()).astype(float)[-1::-1]
            self.nbins = len(self.zpos) + 1
            self.varbins = True
        elif self.nbins == -1:
            self.nbins = int((self.zmax - self.zmin) / dz)


        self.Lz = 0

        self.time = np.zeros(len(self.atomgroup.atoms)).astype(int)-1
        self.msd = np.zeros((self.nbins, self.nframes, 3))
        self.density = np.zeros((self.nbins, self.nframes))

        if self._verbose:
            print('Using', self.nbins, 'bins.')

    def _single_frame(self):

        if (self.zmax == -1):
            zmax = self._ts.dimensions[self.dim]
        else:
            zmax = self.zmax

        if self.membrane_shift:
            # shift membrane
            self._ts.positions[:, self.dim] += self._ts.dimensions[self.dim] / 2
            self._ts.positions[:, self.dim] %= self._ts.dimensions[self.dim]

        if self._frame_index == 0:
            self.positions0 = self.atomgroup.positions

            # 2.3 3.5 same from other side
            if self.varbins:
                # coded bin size
                tmp = (self.atomgroup.positions[:, self.dim] - self.zmin)
                n = int(len(self.zpos))
                self.bins = np.zeros(len(tmp), dtype='int')
                self.bins[tmp >= self.zmax] = n
                self.bins[tmp <= self.zmax] = n
                for k, zpos in enumerate(self.zpos):
                    self.bins[tmp <= zpos] = int(n - k - 1)
            else:
                # locate bins and store
                # vectorized need to verify
                self.bins = ((self.atomgroup.positions[:, self.dim] - self.zmin) /
                           ((zmax - self.zmin) / (self.nbins))).astype(int)
                self.bins[self.bins < 0] = 0
                self.bins[self.bins >= self.nbins] = self.nbins - 1

        # vectorize
        if self.varbins:
            # locate bins
            tmp = (self.atomgroup.positions[:, self.dim] - self.zmin)
            n = int(len(self.zpos))
            bins = np.zeros(len(tmp), dtype='int')
            bins[tmp >= self.zmax] = n
            bins[tmp <= self.zmax] = n
            for k, zpos in enumerate(self.zpos):
                bins[tmp <= zpos] = int(n - k - 1)
        else:
            # locate bins
            bins = ((self.atomgroup.positions[:, self.dim] - self.zmin) /
                         ((zmax - self.zmin) / (self.nbins))).astype(int)
            bins[bins < 0] = 0
            bins[bins >= self.nbins] = self.nbins - 1

        # if same then add time
        # else new bin
        self.time[self.bins == bins] += 1
        self.time[self.bins != bins] = 0

        # need to check that no self.time > self.nframes
        # I would like to say discard, but I need to assign somewhere
        # if I assign to 0 it screws the results.
        # assign to self.nframes and ignore last data point
        self.time[self.time > self.nframes] = self.nframes

        # reassign 0 positions
        self.positions0[self.time == 0] = self.atomgroup.positions[self.time == 0]

        # calculate distance^2
        d2 = (self.atomgroup.positions - self.positions0) ** 2

        # assign to bin with respective time and normalize
        self.msd[bins, self.time, :] += d2
        self.density[bins, self.time] += 1

        # make new self.bins
        self.bins = bins

        # ========================================================

        # # calculate density for normalization
        # bins = ((self.atomgroup.atoms.positions[:, self.dim] - self.zmin) /
        #         ((zmax - self.zmin) / (self.nbins))).astype(int)
        # bins[np.where(bins < 0)] = 0  # put positions back inside box
        # bins[np.where(bins >= self.nbins)] = self.nbins - 1
        # density = np.histogram(bins, bins=np.arange(self.nbins + 1))[0]

        # for j, sel in enumerate(self.atomgroup.atoms):
        #
        #     # locate current atom position
        #     bin_cur = ((sel.position[self.dim] - self.zmin) /
        #             ((zmax - self.zmin) / (self.nbins))).astype(int)
        #     if bin_cur < 0:
        #         bin_cur = 0
        #     elif bin_cur >= self.nbins:
        #         bin_cur = self.nbins - 1
        #
        #     # locate previous atom position
        #     bin_prev = ((self.positions0[j, self.dim] - self.zmin) /
        #             ((zmax - self.zmin) / (self.nbins))).astype(int)
        #     if bin_prev < 0:
        #         bin_prev = 0
        #     elif bin_prev >= self.nbins:
        #         bin_prev = self.nbins - 1
        #
        #     if bin_prev == bin_cur:
        #         # advance in time
        #         self.time[j] += 1
        #     else:
        #         # reset time to 1
        #         self.time[j] = 0
        #         self.positions0[j] = sel.position
        #
        #     # calculate distance^2
        #     d2 = (sel.position - self.positions0[j]) ** 2
        #
        #     # assign to bin with respective time and normalize
        #     self.msd[bin_cur, self.time[j], :] += d2
        #     self.density[bin_cur, self.time[j]] += 1

        self.Lz += self._ts.dimensions[self.dim]

        if self._save and self._frame_index % self.outfreq == 0 and self._frame_index > 0:
            self._calculate_results()
            self._save_results()

    def _calculate_results(self):
        self._index = self._frame_index + 1

        self.results["dens"] = self.density

        self.density[self.density==0] = 1

        self.results["msdx"] = self.msd[:, :, 0] / self.density
        self.results["msdy"] = self.msd[:, :, 1] / self.density
        self.results["msdz"] = self.msd[:, :, 2] / self.density

        if (self.zmax == -1):
            self.results["z"] = np.linspace(self.zmin, self.Lz / self._index,
                                            len(self.results["msdx"]))
        else:
            self.results["z"] = np.linspace(self.zmin, self.zmax,
                                            len(self.results["msdx"]))

    def _save_results(self):
        outdata_dens = np.hstack([
            self.results["z"][:, np.newaxis],
            self.results["dens"][:, :],
        ])
        outdata_msdx = np.hstack([
            self.results["z"][:, np.newaxis],
            self.results["msdx"][:, :],
        ])
        outdata_msdy = np.hstack([
            self.results["z"][:, np.newaxis],
            self.results["msdy"][:, :],
        ])
        outdata_msdz = np.hstack([
            self.results["z"][:, np.newaxis],
            self.results["msdz"][:, :],
        ])

        if (self.bsym):
            for i in range(len(outdata_msdx) - 1):
                outdata_dens[i + 1] = .5 * \
                                     (outdata_dens[i + 1] + outdata_dens[i + 1][-1::-1])
                outdata_msdx[i + 1] = .5 * \
                                     (outdata_msdx[i + 1] + outdata_msdx[i + 1][-1::-1])
                outdata_msdy[i + 1] = .5 * \
                                     (outdata_msdy[i + 1] + outdata_msdy[i + 1][-1::-1])
                outdata_msdz[i + 1] = .5 * \
                                     (outdata_msdz[i + 1] + outdata_msdz[i + 1][-1::-1])

        savetxt("{}{}".format(self.output_prefix, "_dens"),
                outdata_dens,
                header='dens')
        savetxt("{}{}".format(self.output_prefix, "_msdx"),
                outdata_msdx,
                header='msdx')
        savetxt("{}{}".format(self.output_prefix, "_msdy"),
                outdata_msdy,
                header='msdy')
        savetxt("{}{}".format(self.output_prefix, "_msdz"),
                outdata_msdz,
                header='msdz')


#@charge_neutral(filter="default")
class msd_planar8(SingleGroupAnalysisBase):
    """
    """

    def __init__(self,
                 atomgroups,
                 output_prefix="msd",
                 binwidth=0.05,
                 nbins=-1,
                 dim=2,
                 zmin=0,
                 zmax=-1,
                 outfreq=10000,
                 b2d=False,
                 bsym=False,
                 vac=False,
                 membrane_shift=False,
                 com=False,
                 bpbc=True,
                 varbins=False,
                 wallwidth=-1,
                 sternlayer=-1,
                 **kwself):
        super().__init__(atomgroups, **kwself)
        self.output_prefix = output_prefix
        self.binwidth = binwidth
        self.dim = dim
        self.zmin = zmin
        self.zmax = zmax
        self.outfreq = outfreq
        self.b2d = b2d
        self.bsym = bsym
        self.vac = vac
        self.membrane_shift = membrane_shift
        self.nbins = nbins
        self.varbins = varbins
        self.wallwidth = wallwidth
        self.sternlayer = sternlayer
        # print('In development. EXITING')
        # exit()

    def _configure_parser(self, parser):
        parser.add_argument('-o', dest='output_prefix')
        parser.add_argument('-dz', dest='binwidth')
        parser.add_argument('-nbins', dest='n_bins')
        parser.add_argument('-d', dest='dim')
        parser.add_argument('-zmin', dest='zmin')
        parser.add_argument('-zmax', dest='zmax')
        parser.add_argument('-temp', dest='temperature')
        parser.add_argument('-dout', dest='outfreq')
        parser.add_argument('-2d', dest='b2d')
        parser.add_argument('-vac', dest='vac')
        parser.add_argument('-sym', dest='bsym')
        parser.add_argument('-shift', dest='membrane_shift')
        parser.add_argument('-varbins', dest='varbins')
        parser.add_argument('-wallwidth', dest='wallwidth')
        parser.add_argument('-sternlayer', dest='sternlayer')

    def _prepare(self):
        if self._verbose:
            print("\nCalcualate profile for the following group(s):")

        self.zmax = float(self.zmax)
        self.zmin = float(self.zmin)
        self.binwidth = float(self.binwidth)
        self.nbins = int(self.nbins)
        self.dim = int(self.dim)
        self.outfreq = 10000
        self.wallwidth = float(self.wallwidth)
        self.sternlayer = float(self.sternlayer)

        # Assume a threedimensional universe...
        self.xydims = np.roll(np.arange(3), -self.dim)[1:]
        dz = self.binwidth

        if self.zmax == -1:
            self.zmax = self._universe.dimensions[self.dim]

        # CAVE: binwidth varies in NPT !
        # Use fix bin width if no nbins are specified
        if self.nbins == -1:
            self.nbins = int((self.zmax - self.zmin) / dz)

        self.Lz = 0
        self.time = np.zeros(len(self.atomgroup.atoms)).astype(int) - 1
        self.msd = np.zeros((self.nbins, self.n_frames, 3))
        self.density = np.zeros((self.nbins, self.n_frames))
        if self._verbose:
            print('Using', self.nbins, 'bins.')

    def _single_frame(self):

        if (self.zmax == -1):
            zmax = self._ts.dimensions[self.dim]
        else:
            zmax = self.zmax

        if self.membrane_shift:
            # shift membrane
            self._ts.positions[:, self.dim] += self._ts.dimensions[self.dim] / 2
            self._ts.positions[:, self.dim] %= self._ts.dimensions[self.dim]

        if self._frame_index == 0:
            self.positions0 = self.atomgroup.positions

        # ========================================================

        # # calculate density for normalization
        # bins = ((self.atomgroup.atoms.positions[:, self.dim] - self.zmin) /
        #         ((zmax - self.zmin) / (self.nbins))).astype(int)
        # bins[np.where(bins < 0)] = 0  # put positions back inside box
        # bins[np.where(bins >= self.nbins)] = self.nbins - 1
        # density = np.histogram(bins, bins=np.arange(self.nbins + 1))[0]
        ######## AANGEPAST DOOR FENNA ########
        if self.varbins:
            for j, sel in enumerate(self.atomgroup.atoms):
                # locate current atom position
                if (sel.position[self.dim] - self.zmin) <= self.wallwidth:
                    bin_cur = 0
                elif (sel.position[self.dim] - self.zmin) <= (self.wallwidth + self.sternlayer):
                    bin_cur = 1
                elif (sel.position[self.dim] - self.zmin) <= (self.wallwidth + self.sternlayer) + (
                        (zmax - self.wallwidth - self.sternlayer) - (self.wallwidth + self.sternlayer)) / 4:
                    bin_cur = 2
                elif (sel.position[self.dim] - self.zmin) <= (self.wallwidth + self.sternlayer) + (
                        (zmax - self.wallwidth - self.sternlayer) - (self.wallwidth + self.sternlayer)) / 2:
                    bin_cur = 3
                elif (sel.position[self.dim] - self.zmin) <= (self.wallwidth + self.sternlayer) + (
                        (zmax - self.wallwidth - self.sternlayer) - (self.wallwidth + self.sternlayer)) * 3 / 4:
                    bin_cur = 4
                elif (sel.position[self.dim] - self.zmin) <= (zmax - self.wallwidth - self.sternlayer):
                    bin_cur = 5
                elif (sel.position[self.dim] - self.zmin) <= (zmax - self.wallwidth):
                    bin_cur = 6
                else:
                    bin_cur = 7

                # locate previous atom position
                if (self.positions0[j, self.dim] - self.zmin) <= self.wallwidth:
                    bin_prev = 0
                elif (self.positions0[j, self.dim] - self.zmin) <= (self.wallwidth + self.sternlayer):
                    bin_prev = 1
                elif (self.positions0[j, self.dim] - self.zmin) <= (self.wallwidth + self.sternlayer) + (
                        (zmax - self.wallwidth - self.sternlayer) - (self.wallwidth + self.sternlayer)) / 4:
                    bin_prev = 2
                elif (self.positions0[j, self.dim] - self.zmin) <= (self.wallwidth + self.sternlayer) + (
                        (zmax - self.wallwidth - self.sternlayer) - (self.wallwidth + self.sternlayer)) / 2:
                    bin_prev = 3
                elif (self.positions0[j, self.dim] - self.zmin) <= (self.wallwidth + self.sternlayer) + (
                        (zmax - self.wallwidth - self.sternlayer) - (self.wallwidth + self.sternlayer)) * 3 / 4:
                    bin_prev = 4
                elif (self.positions0[j, self.dim] - self.zmin) <= (zmax - self.wallwidth - self.sternlayer):
                    bin_prev = 5
                elif (self.positions0[j, self.dim] - self.zmin) <= (zmax - self.wallwidth):
                    bin_prev = 6
                else:
                    bin_prev = 7
                ######################################

                if bin_prev == bin_cur:
                    # advance in time
                    self.time[j] += 1
                else:
                    # reset time to 1
                    self.time[j] = 0
                    self.positions0[j] = sel.position

                # calculate distance^2
                d2 = (sel.position - self.positions0[j]) ** 2

                # assign to bin with respective time and normalize
                self.msd[bin_cur, self.time[j], :] += d2
                self.density[bin_cur, self.time[j]] += 1

        else:
            for j, sel in enumerate(self.atomgroup.atoms):
                # locate current atom position
                bin_cur = ((sel.position[self.dim] - self.zmin) /
                           ((zmax - self.zmin) / (self.nbins))).astype(int)
                if bin_cur < 0:
                    bin_cur = 0
                elif bin_cur >= self.nbins:
                    bin_cur = self.nbins - 1

                # locate previous atom position
                bin_prev = ((self.positions0[j, self.dim] - self.zmin) /
                            ((zmax - self.zmin) / (self.nbins))).astype(int)
                if bin_prev < 0:
                    bin_prev = 0
                elif bin_prev >= self.nbins:
                    bin_prev = self.nbins - 1

                if bin_prev == bin_cur:
                    # advance in time
                    self.time[j] += 1
                else:
                    # reset time to 1
                    self.time[j] = 0
                    self.positions0[j] = sel.position

                # calculate distance^2
                d2 = (sel.position - self.positions0[j]) ** 2

                # assign to bin with respective time and normalize
                self.msd[bin_cur, self.time[j], :] += d2
                self.density[bin_cur, self.time[j]] += 1

        self.Lz += self._ts.dimensions[self.dim]

        if self._save and self._frame_index % self.outfreq == 0 and self._frame_index > 0:
            self._calculate_results()
            self._save_results()

    def _calculate_results(self):
        self._index = self._frame_index + 1

        self.results["msdx"] = np.nan_to_num(self.msd[:, :, 0] / self.density)
        self.results["msdy"] = np.nan_to_num(self.msd[:, :, 1] / self.density)
        self.results["msdz"] = np.nan_to_num(self.msd[:, :, 2] / self.density)

        if (self.zmax == -1):
            self.results["z"] = np.linspace(self.zmin, self.Lz / self._index,
                                            len(self.results["msdx"]))
        else:
            self.results["z"] = np.linspace(self.zmin, self.zmax,
                                            len(self.results["msdx"]))

    def _save_results(self):
        outdata_msdx = np.hstack([
            self.results["z"][:, np.newaxis],
            self.results["msdx"][:, :],
        ])
        outdata_msdy = np.hstack([
            self.results["z"][:, np.newaxis],
            self.results["msdy"][:, :],
        ])
        outdata_msdz = np.hstack([
            self.results["z"][:, np.newaxis],
            self.results["msdz"][:, :],
        ])

        if (self.bsym):
            for i in range(len(outdata_msdx) - 1):
                outdata_msdx[i + 1] = .5 * \
                                      (outdata_msdx[i + 1] + outdata_msdx[i + 1][-1::-1])
                outdata_msdy[i + 1] = .5 * \
                                      (outdata_msdy[i + 1] + outdata_msdy[i + 1][-1::-1])
                outdata_msdz[i + 1] = .5 * \
                                      (outdata_msdz[i + 1] + outdata_msdz[i + 1][-1::-1])

        savetxt("{}{}".format(self.output_prefix, "_msdx"),
                outdata_msdx,
                header='msdx')
        savetxt("{}{}".format(self.output_prefix, "_msdy"),
                outdata_msdy,
                header='msdy')
        savetxt("{}{}".format(self.output_prefix, "_msdz"),
                outdata_msdz,
                header='msdz')
