#!/usr/bin/env python
# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding:utf-8 -*-
#
# Copyright (c) 2020 Authors and contributors
# (see the file AUTHORS for the full list of names)
#
# Released under the GNU Public Licence, v2 or any higher version
# SPDX-License-Identifier: GPL-2.0-or-later

'''
This class is implemented as part of the expansion to MAICOS.
It does not use GroupAnalysisBase nor MDanalysis.
Despite that, for consistency these are read to provide MAICoS support.

Simulations to be analyzed should have the following structure
case_folder/lambda_coul_0.0000
case_folder/lambda_vdw_0.0000
with increasing numbers and always 4 digits.

Files to be read in are
case_folder/in.lambdas
case_folder/lambda_<>/out.solvation

the in.lambdas file includes first coul stage followed by vdw stage as
0.0 ... 1.0 0.0 ... 1.0 or vice virsa

'''

import numpy as np
import scipy.constants
import scipy.interpolate
import MDAnalysis as mda

from .base import SingleGroupAnalysisBase, MultiGroupAnalysisBase
from ..utils import check_compound, FT, iFT, savetxt
from ..decorators import charge_neutral

def read_file(filename):

    header = []
    data = []

    with open(filename, 'r') as f:
        for line in f:
            if line.startswith('#'):
                header.append(line)
            else:
                data.append([float(elem) for elem in line.rstrip('\n').split(' ')])

    return np.asarray(data), header

def normal(data):

    if type(data) == np.ndarray:

        N = len(data)
        mu = np.sum(data) / N
        s = np.sqrt(1 / N * np.sum((data - mu) ** 2))
        s /= np.sqrt(N)

    else:
        mu = data
        s = 0

    return np.array([mu, s])

def fit(x, y, xnew, s=0):

    if x[0] > x[-1]:
        tck = scipy.interpolate.splrep(x[::-1], y[::-1], s=s)
        ynew = scipy.interpolate.splev(xnew[::-1], tck)[::-1]
    else:
        tck = scipy.interpolate.splrep(x, y, s=s)
        ynew = scipy.interpolate.splev(xnew, tck)

    return ynew

def integral(x, y, rule='simps'):

    from scipy.integrate import trapz
    from scipy.integrate import simps

    if rule == 'trapz':
        return trapz(y, x)

    elif rule == 'simps':
        return simps(y, x)


# @charge_neutral(filter="default")
class mu_bulk(SingleGroupAnalysisBase):
    """
    """

    def __init__(self,
                 atomgroup,
                 outfreq=10000,
                 output_prefix="mu",
                 delta=0.002,
                 nstart=0,
                 nend=-1,
                 nbins = 100,
                 folder="",
                 temperature=298,
                 kB=0.001985875,
                 **kwargs):
        super().__init__(atomgroup, **kwargs)
        self.outfreq = outfreq
        self.output_prefix = output_prefix
        self.delta = delta
        self.nstart = nstart
        self.nend = nend
        self.nbins = nbins

        self.folder = folder

        self.temperature = temperature
        self.kB = kB

    def _configure_parser(self, parser):
        parser.add_argument('-o', dest='output_prefix')
        parser.add_argument('-nstart', dest='nstart', default=0)
        parser.add_argument('-nend', dest='nend', default=-1)
        parser.add_argument('-nbins', dest='nbins', default=100)

        parser.add_argument('-folder', dest='folder')
        parser.add_argument('-delta', dest='delta', default=0.002)


    def _prepare(self):

        # # -----------------------------------------------------------
        # # read energies
        # # this method only works in a solid
        # self.lambdas = read_file(self.folder + '/in.lambdas')[0]
        # self.N = len(self.atomgroup)
        # self.raw = np.zeros((len(self.lambdas), 1))
        #
        # # assume Coul stage first
        # stage='coul'
        # c = 0
        # for i, lam in enumerate(self.lambdas):
        #     if i > 0 and (lam == 0.0 or lam == 1.0) and stage == 'coul':
        #         self.idx = i + 1
        #         stage = 'vdw'
        #         c += 1
        #         if c == 2:
        #             print('Value Error')
        #             exit()
        #
        #     data = read_file(self.folder + '/lambda_%s_%.4f/out.TotalE' % (stage, lam))[0]
        #     n_samples = len(data[:, 0])
        #     data[:, 1] /= self.N
        #
        #     # calculate number of samples
        #     if self.nend == -1 or self.nend > n_samples:
        #         nend = n_samples - self.nstart
        #     else:
        #         nend = self.nend
        #     if i > 0 and lam != 0.0:
        #         U1_U0 = data[self.nstart:nend, 1] - U0
        #         exp_U1_U0 = normal(np.exp(-U1_U0 / (self.kB * self.temperature)))[0]
        #         self.raw[i, 0] = np.log(exp_U1_U0)
        #     U0 = data[self.nstart:nend, 1]
        #
        # self.A = - np.sum(self.raw) * self.kB * self.temperature
        # print(self.A)

        # -----------------------------------------------------------
        # read energies
        self.lambdas = read_file(self.folder + '/in.lambdas')[0]

        self.raw = np.zeros((len(self.lambdas), 5))
        self.vol = np.zeros(2)

        # assume Coul stage first
        stage='coul'
        c = 0
        for i, lam in enumerate(self.lambdas):
            if i > 0 and (lam == 0.0 or lam == 1.0) and stage == 'coul':
                self.idx = i + 1
                stage = 'vdw'
                c += 1
                if c == 2:
                    print('Value Error')
                    exit()

            data = read_file(self.folder + '/lambda_%s_%.4f/out.solvation' % (stage, lam))[0]
            n_samples = len(data[:, 0])
            # data[:, 1] /= self.N

            # calculate number of samples
            if self.nend == -1 or self.nend > n_samples:
                nend = n_samples - self.nstart
            else:
                nend = self.nend

            vol = normal(data[self.nstart:nend, 3])
            self.vol[0] += vol[0]
            self.vol[1] += vol[1]

            Gex = normal(data[self.nstart:nend, 1] / self.delta)

            self.raw[i, 0] = lam
            self.raw[i, 1] = Gex[0]  # mu (mean)
            self.raw[i, 2] = Gex[1]  # sigma/sqrt(N) (standard deviation)
            self.raw[i, 3] = vol[0]  # mu
            self.raw[i, 4] = vol[1]  # error=sigma/sqrt(N) (see function normal())

        # self.raw = self.raw[::-1]
        self.vol[0] /= i+1
        self.vol[1] /= i+1

        # -----------------------------------------------------------
        # interpolate results
        xnew = np.linspace(0, 1, self.nbins)

        # initialize results arrays
        self.interp = np.zeros((int(len(xnew)), 5))

        # interpolate mean and deviation using splines
        ynew_coul = fit(self.raw[:self.idx:, 0], self.raw[:self.idx, 1], xnew)  # , data[:idx, 2])
        ynew_vdw = fit(self.raw[self.idx:, 0], self.raw[self.idx:, 1], xnew)  # , data[idx:, 2])

        s_ynew_coul = fit(self.raw[:self.idx, 0], self.raw[:self.idx, 2], xnew)
        s_ynew_vdw = fit(self.raw[self.idx:, 0], self.raw[self.idx:, 2], xnew)

        # write results
        self.interp[:, 0] = xnew
        self.interp[:, 1] = ynew_vdw
        self.interp[:, 2] = s_ynew_vdw
        self.interp[:, 3] = ynew_coul
        self.interp[:, 4] = s_ynew_coul

        # -----------------------------------------------------------
        # integrate over interpolated data [1/kBT]
        self.Gex_vdw = np.zeros(2)
        self.Gex_vdw[0] = integral(self.interp[:, 0], self.interp[:, 1])
        self.Gex_vdw[1] = np.sqrt(integral(self.interp[:, 0], self.interp[:, 2] ** 2))  # /num)

        self.Gex_coul = np.zeros(2)
        self.Gex_coul[0] = integral(self.interp[:, 0], self.interp[:, 3])
        self.Gex_coul[1] = np.sqrt(integral(self.interp[:, 0], self.interp[:, 4] ** 2))  # /num)
        # end integrate

    def _single_frame(self):
        # do nothing

        if self._save and self._frame_index % self.outfreq == 0 and self._frame_index > 0:
            self._calculate_results()
            self._save_results()

    def _calculate_results(self):

        self.results = {}
        self.results['Gex_coul'] = self.Gex_coul
        self.results['Gex_vdw'] = self.Gex_vdw
        self.results['vol'] = self.vol[0]
        # self.results['A'] = self.A

    def _conclude(self):
        if self._verbose:
            print("Solvation energies:")
            print("Gex_coul: %.4f" % (self.Gex_coul[0]))
            print("Gex_vdw: %.4f" % (self.Gex_vdw[0]))
            print("vol: %.4f" % (self.vol[0]))
            # print('Gex: %.4f' % self.A)

    def _save_results(self):

        savetxt("{}{}".format(self.output_prefix, "_raw"),
                self.raw,
                header='#\lambda,G_solv,e,vol,e')

        savetxt("{}{}".format(self.output_prefix, "_interp"),
                self.interp,
                header='#\lambda,G_vdw,e,G_coul,e')




class hooks_law(SingleGroupAnalysisBase):
    """
    Calculates msd based on spring energy for solid chemical potential
    Integration needs to be afterwards
    """

    def __init__(self,
                 atomgroup,
                 outfreq=10000,
                 output="msd",
                 nstart=0,
                 nend=-1,
                 folder="",
                 **kwargs):
        super().__init__(atomgroup, **kwargs)
        self.outfreq = outfreq
        self.output = output
        self.nstart = nstart
        self.nend = nend

        self.folder = folder

    def _configure_parser(self, parser):
        parser.add_argument('-o', dest='output_prefix')
        parser.add_argument('-ns', dest='nstart')
        parser.add_argument('-ne', dest='nend')

        parser.add_argument('-folder', dest='folder')

    def _prepare(self):

        self.k = read_file(self.folder + '/in.k')[0]

        self.landakt = np.zeros(len(self.k[:, 0]))
        self.msd = np.zeros((len(self.k[:, 0]), 2))

        for i, num, k, kmax in enumerate(self.k):

            data = read_file(self.folder + '/UPosRest_%.4f/out.UPosRest' % k)[0]
            n_samples = len(data[:, 0])

            # calculate number of samples
            if self.nend == -1 or self.nend > n_samples:
                nend = n_samples - self.nstart
            else:
                nend = self.nend

            data_ave = normal(data[self.nstart:nend, 1])

            # obtain actual landa
            print("WARNING: divides by 2 because we consider LAMMPS only")
            self.landakt[i] = k / 2

            # compute <MSD>_landa_i
            self.msd[i, 0] = data_ave[0] / self.landakt[i]
            self.msd[i, 1] = data_ave[1] / self.landakt[i]

        # deltaA2 = numpy.zeros(2)
        # print(np.sum(MSD[:, 0] * (landakt + np.exp(x))) * np.log(1 + landakt[-1] / np.exp(x)) / len(landakt))
        # deltaA2[0] = - numpy.sum(w.T * MSD[:, 0] * (landakt + numpy.exp(x)))
        # deltaA2[1] = - numpy.sqrt(numpy.sum((SD[:, 1] * (landakt + numpy.exp(x))) ** 2)) / numpy.sqrt(
        #     len(data[:, 1]))
        # print('A2 = ', deltaA2)




    def _single_frame(self):
        # do nothing

        if self._save and self._frame_index % self.outfreq == 0 and self._frame_index > 0:
            self._calculate_results()
            self._save_results()

    def _calculate_results(self):
        self.results = {}
        self.results['k'] = self.landakt
        self.results['msd'] = self.msd[:, 0]
        self.results['dmsd'] = self.msd[:, 1]


    def _conclude(self):
        if self._verbose:
            print("Nothing here")

    def _save_results(self):

        outdata = np.hstack([
            self.results["k"][:, np.newaxis],
            self.results["msd"][:, np.newaxis],
            self.results["dmsd"][:, np.newaxis],
        ])

        savetxt(self.output,
                outdata,
                header='k,msd,dmsd')



