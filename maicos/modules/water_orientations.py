#!/usr/bin/env python
# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding:utf-8 -*-
#
# Copyright (c) 2020 Authors and contributors
# (see the file AUTHORS for the full list of names)
#
# Released under the GNU Public Licence, v2 or any higher version
# SPDX-License-Identifier: GPL-2.0-or-later

import numpy as np
import scipy.constants
import MDAnalysis as mda

from .base import SingleGroupAnalysisBase, MultiGroupAnalysisBase
from ..utils import check_compound, FT, iFT, savetxt
from ..decorators import charge_neutral


@charge_neutral(filter="error")
class water_orientation(SingleGroupAnalysisBase):
    """
    """

    def __init__(self,
                 atomgroups,
                 output="water_orientation.dat",
                 binwidth=0.05,
                 dim=2,
                 zmin=0,
                 zmax=-1,
                 temperature=300,
                 outfreq=10000,
                 b2d=False,
                 bsym=False,
                 vac=False,
                 membrane_shift=False,
                 com=False,
                 bpbc=True,
                 nbins=-1,
                 **kwself):
        super().__init__(atomgroups, **kwself)
        self.output = output
        self.binwidth = binwidth
        self.nbins = nbins
        self.dim = dim
        self.zmin = zmin
        self.zmax = zmax
        self.temperature = temperature
        self.outfreq = outfreq
        self.b2d = b2d
        self.bsym = bsym
        self.vac = vac
        self.membrane_shift = membrane_shift
        self.com = com
        self.bpbc = bpbc

    def _configure_parser(self, parser):
        parser.add_argument('-o', dest='output', default='cos_theta.dat')
        parser.add_argument('-dz', dest='binwidth', default=0.5)
        parser.add_argument('-nbins', dest='nbins', default=-1)
        parser.add_argument('-d', dest='dim', default=2)
        parser.add_argument('-zmin', dest='zmin', default=0)
        parser.add_argument('-zmax', dest='zmax', default=-1)
        parser.add_argument('-temp', dest='temperature', default=300)
        parser.add_argument('-dout', dest='outfreq', default=100000000)
        parser.add_argument('-2d', dest='b2d', default=False)
        parser.add_argument('-vac', dest='vac', default=False)
        parser.add_argument('-sym', dest='bsym', default=False)
        parser.add_argument('-shift', dest='membrane_shift', default=True)
        parser.add_argument('-com', dest='com', default=False)
        parser.add_argument('-nopbcrepair', dest='bpbc', default=True)


    def _prepare(self):
        if self._verbose:
            print("\nCalcualate profile for the following group(s):")

        if self.com:
            try:
                self.sol = self._universe.select_atoms('resname SOL')
            except AttributeError:
                raise AttributeError("No residue information."
                                     "Cannot apply water COM shift.")

            if len(self.sol) == 0:
                raise ValueError("No atoms for water COM shift found.")

        # Assume a threedimensional universe...
        self.xydims = np.roll(np.arange(3), -self.dim)[1:]
        dz = self.binwidth #* 10  # Convert to Angstroms

        if self.zmax == -1:
            self.zmax = self._universe.dimensions[self.dim]
        # else:
        #     self.zmax *= 10

        # self.zmin *= 10
        # CAVE: binwidth varies in NPT !
        if self.nbins == -1:
            self.nbins = int((self.zmax - self.zmin) / dz)

        self.V = 0
        self.Lz = 0
        self.A = np.prod(self._universe.dimensions[self.xydims])

        self.unit_vector = np.zeros(3)
        self.unit_vector[self.dim] += 1

        self.density = np.zeros((self.nbins))

        # Same for perpendicular
        self.cos_theta = np.zeros(self.nbins)
        self.cos_theta2 = np.zeros(self.nbins)

        if self._verbose:
            print('Using', self.nbins, 'bins.')

    def _single_frame(self):

        if (self.zmax == -1):
            zmax = self._ts.dimensions[self.dim]
        else:
            zmax = self.zmax

        if self.membrane_shift:
            # shift membrane
            self._ts.positions[:, self.dim] += self._ts.dimensions[self.dim] / 2
            self._ts.positions[:, self.dim] %= self._ts.dimensions[self.dim]

        if self.com:
            # put water COM into center
            waterCOM = np.sum(
                self.sol.atoms.positions[:, self.dim] *
                self.sol.atoms.masses) / self.sol.atoms.masses.sum()
            if self._verbose:
                print("shifting by ", waterCOM)
            self._ts.positions[:, self.dim] += self._ts.dimensions[
                                                   self.dim] / 2 - waterCOM
            self._ts.positions[:, self.dim] %= self._ts.dimensions[self.dim]

        if self.bpbc:
            # make broken molecules whole again!
            self._universe.atoms.unwrap(compound=check_compound(self._universe.atoms))

        dz_frame = self._ts.dimensions[self.dim] / self.nbins

        # find the bins
        Ow = np.where(self.atomgroup.atoms.types == '1')[0]

        bins = ((self.atomgroup.atoms.positions[Ow, self.dim] - self.zmin) /
                ((zmax - self.zmin) / (self.nbins))).astype(int)
        bins[np.where(bins < 0)] = 0  # put all charges back inside box
        bins[np.where(bins >= self.nbins)] = self.nbins - 1
        self.density += np.histogram(bins, bins=np.arange(self.nbins + 1))[0]
        # g[g==0] = 1

        vec = np.zeros((len(Ow), 3))
        vec += self.atomgroup.atoms.positions[Ow + 1, :] - self.atomgroup.atoms.positions[Ow, :]
        vec += self.atomgroup.atoms.positions[Ow + 2, :] - self.atomgroup.atoms.positions[Ow, :]

        cos_theta = np.dot(vec, self.unit_vector) / np.sqrt(np.sum(vec ** 2, axis=1))

        inds = np.searchsorted(np.arange(self.nbins + 1), bins)
        # inds = np.digitize(bins, np.arange(self.nbins + 1))-1

        this_cos_theta = np.zeros(self.nbins)
        this_cos_theta[inds] += cos_theta
        # this_cos_theta /= g

        self.cos_theta += this_cos_theta
        self.cos_theta2 += this_cos_theta ** 2

        self.V += self._ts.volume
        self.Lz += self._ts.dimensions[self.dim]

        if self._save and self._frame_index % self.outfreq == 0 and self._frame_index > 0:
            self._calculate_results()
            self._save_results()

    def _calculate_results(self):
        self._index = self._frame_index + 1
        self.density[self.density==0]=1

        self.results["V"] = self.V / self._index
        self.results["cos_theta"] = self.cos_theta / self.density
        self.results["dcos_theta"] = self.cos_theta2 / self.density

        if (self.zmax == -1):
            self.results["z"] = np.linspace(self.zmin, self.Lz / self._index,
                                            len(self.results["cos_theta"])) #/ 10
        else:
            self.results["z"] = np.linspace(self.zmin, self.zmax,
                                            len(self.results["cos_theta"])) #/ 10

    def _save_results(self):
        outdata = np.hstack([
            self.results["z"][:, np.newaxis],
            self.results["cos_theta"][:, np.newaxis],
            self.results["dcos_theta"][:, np.newaxis],
        ])

        if (self.bsym):
            for i in range(len(outdata) - 1):
                outdata[i + 1] = .5 * \
                                     (outdata[i + 1] + outdata[i + 1][-1::-1])

        savetxt(self.output,
                outdata,
                header="z,cos_theta,dcos_theta")
